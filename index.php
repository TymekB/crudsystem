<?php
    require "backend/customer.php";
?>
<!DOCTYPE HTML>

<html lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>CRUD System</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    
</head>

<body>

<div class="container">
    <article>
        <header>

            <h1 title="Create reade update and delete System">PHP CRUD System</h1>
            <hr>
        </header>
        
        <section>

            <header>
                <h3>Customers</h3>
            </header>

            <div class="panel panel-default">

                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    <?php

                        $customer = new Customer();

                        $customers = $customer->getCustomers();
                        
                        foreach($customers as $customer)
                        {
                            echo '<tr>';
                            echo '<td>'.$customer->id.'</td>';
                            echo '<td>'.$customer->name.'</td>';
                            echo '<td>'.$customer->surname.'</td>';
                            echo '<td>'.$customer->email.'</td>';
                            echo '<td><a href="update.php?id='.$customer->id.'" class="btn btn-success btn-small">Edit</a> <a href="backend/delete.php?id='.$customer->id.'" class="btn btn-danger btn-small">Delete</a></td>';
                        }

                    ?>
        
                    </tbody>
                </table>
            </div>
        </section>

        <a href="create.php" class="btn btn-primary">Add customer</a>

    </article>
</div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
</body>

</html>