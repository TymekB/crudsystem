<?php
    session_start();
    require "backend/customer.php";

    if(isset($_POST['name']))
    {
        $customer = new Customer();
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $validationOk = true;

        // Name validation
        if(strlen($name) < 3 || strlen($name) > 20)
        {
            $_SESSION['e_name'] = "Name is too short or too long! (3-20)";
            $validationOk = false;
        }

        if(!ctype_alnum($name))
        {
            $_SESSION['e_name'] = "Invalid name!";
            $validationOk = false;
        }
        
        // Surname validation
        if(strlen($surname) < 3 || strlen($surname) > 20)
        {
            $_SESSION['e_surname'] = "Surname is too short or too long! (3-20)";
            $validationOk = false;
        }

        if(!ctype_alnum($surname))
        {
            $_SESSION['e_name'] = "Invalid name!";
            $validationOk = false;
        }
        
        // Email validation
        $sanitizedEmail = filter_var($email, FILTER_SANITIZE_EMAIL);
        
        if(!filter_var($sanitizedEmail, FILTER_VALIDATE_EMAIL) || $sanitizedEmail != $email)
        {
            $_SESSION['e_email'] = "Email is invalid!";
            $validationOk = false;
        }

        if($customer->searchEmail($email))
        {
            $_SESSION['e_email'] = "Email is already taken!";
            $validationOk = false;
        }

        if($validationOk)
        {
            $customer->addCustomer($name, $surname, $email);

            header("Location: index.php");
        }
    }
?>
<!DOCTYPE HTML>

<html lang="eng">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>CRUD System</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    
</head>

<body>

<div class="container">
    <article>
        <header>

            <h1 title="Create read update and delete System">PHP CRUD System</h1>
            <hr>
        </header>
        
        <section>
            <header>
                <h3>Add customer</h3>
            </header>

            <form class="form-horizontal" method="post">
                    <div class="control-group">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input name="name" type="text">
                        </div>
                    </div>
        
                    <div class="control-group">
                        <label class="control-label">Surname</label>
                        <div class="controls">
                            <input name="surname" type="text">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label">Email Address</label>
                        <div class="controls">
                            <input name="email" type="text">
                        </div>
                    </div>
                    
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Create</button>
                        <a class="btn" href="index.php">Back</a>
                    </div>
              </form>

              <?php

                if(isset($_SESSION['e_name']))
                {
                    echo '<div class="alert alert-danger">';
                    echo $_SESSION['e_name'];
                    echo '</div>';

                    unset($_SESSION['e_name']);
                }

                if(isset($_SESSION['e_surname']))
                {
                    echo '<div class="alert alert-danger">';
                        echo $_SESSION['e_surname'];
                    echo '</div>';

                    unset($_SESSION['e_surname']);
                }

                if(isset($_SESSION['e_email']))
                {
                    echo '<div class="alert alert-danger">';
                        echo $_SESSION['e_email'];
                    echo '</div>';

                    unset($_SESSION['e_email']);
                }

              ?>
            
        </section>


    </article>
</div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
</body>

</html>