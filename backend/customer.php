<?php

class Customer 
{
    private $pdo;

    function __construct()
    {
        require "config.php";
        
        $this->pdo = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $db_password);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function getCustomers()
    {
        $customers = [];

        try
        {
            $stmt = $this->pdo->query("SELECT * FROM customers");

            $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        }
        catch(PDOException $e)
        {
            echo $e;
        }

        return $customers;
    }

    public function addCustomer($name, $surname, $email)
    {
        try
        {
            $stmt = $this->pdo->prepare("INSERT INTO customers VALUES(null, :name, :surname, :email)");

            $stmt->execute(['name' => $name, 'surname' => $surname, 'email' => $email]);
        }
        catch(PDOException $e)
        {
            echo $e;
        }
    }

    public function deleteCustomer($id)
    {
        try
        {
            $stmt = $this->pdo->prepare("DELETE FROM customers WHERE id=:id");

            $stmt->execute(['id'=>$id]);
        }
        catch(PDOException $e)
        {
            echo $e;
        }
    }

    public function updateCustomer($id, $name, $surname, $email)
    {
        try
        {   
            $stmt = $this->pdo->prepare("UPDATE customers SET name=:name, surname=:surname, email=:email WHERE id=:id");

            $stmt->execute(['id'=>$id, 'name'=>$name, 'surname'=>$surname, 'email'=>$email]);
        }
        catch(PDOException $e)
        {
            echo $e;
        }
    }

    public function searchCustomer($id)
    {
        $customer = false;

        try
        {   
            $stmt = $this->pdo->prepare("SELECT * FROM customers WHERE id=:id");

            $stmt->execute(['id'=>$id]);
            
            $customer = $stmt->fetch(PDO::FETCH_OBJ);
        }
        catch(PDOException $e)
        {
            echo $e;
        } 

        return $customer;
    }

    public function searchEmail($email)
    {
        $emailFound = false;
        
        try
        {   
            $stmt = $this->pdo->prepare("SELECT * FROM customers WHERE email=:email");

            $stmt->execute(['email'=>$email]);

            if($stmt->rowCount() > 0)
            {
                $emailFound = true;
            }
        }
        catch(PDOException $e)
        {
            echo $e;
        } 

        return $emailFound;
    }

}