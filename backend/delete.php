<?php

    require "customer.php";

    if(isset($_GET['id']))
    {
        $id = $_GET['id'];
        $customer = new Customer();

        $customer->deleteCustomer($id);
        
    }

    header("Location: ../index.php");